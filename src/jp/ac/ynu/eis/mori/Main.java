package jp.ac.ynu.eis.mori;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.XmlStreamReader;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 *
 */
public class Main {
    public static void main(String[] args){
        System.out.println("Java IO Libraries - Practice");
        changeEncoding();
        encodingPractice();
        java7PathPractice();
        URLDownloadPractice();
    }

    public static void changeEncoding(){
        try{
            System.out.println("---- changing encoding ----");
            String content = FileUtils.readFileToString(new File("./save/src.txt"), "EUC-JP");
            FileUtils.writeStringToFile(new File("./save/dst.txt"), content, "Shift-JIS");
            System.out.println("file saved.");
        }catch(IOException e){
            System.out.println(e);
        }
    }

    public static void encodingPractice(){
        System.out.println("---- Encoding Practice ----");
        System.out.println("default encoding : " + System.getProperty("file.encoding"));
        System.out.println("コンソール文字化けチェック（人力）");
    }


    public static void URLDownloadPractice(){
        // 参考：http://commons.apache.org/proper/commons-io/description.html
        System.out.println("---- URL Download Practice ---");
        XmlStreamReader reader = null;
        try{
            // URLから文字列へ
            InputStream in = new URL( "http://commons.apache.org/proper/commons-io/javadocs/api-2.4/org/apache/commons/io/input/XmlStreamReader.html" ).openStream();// Webから引いてきたInputStream
            reader = new XmlStreamReader(in);
            System.out.println( "default toString method: " + reader); // ← 失敗例。これだけでは中身は分からない
            String htmlStr = IOUtils.toString(reader); // IOUtilsなら、一行でStringに変換できる。
            System.out.println("encoding : " + reader.getEncoding()); // XmlStreamReaderは、xmlデータから文字エンコードを推測する。
            System.out.println( "IOUtils toString method: ... " + htmlStr.substring(1000, 2500).replace('\n', ' ') );
            // 注：読み込むファイルが重くなると、それだけ重いStringができるので注意。この方法は小さいファイル向け。

            // 文字列からファイルへ
            File fSaveTo = new File("./save/downloaded.txt");
            FileUtils.forceMkdir(fSaveTo.getParentFile()); // saveフォルダがない場合に作成。標準のFileクラスにも似たメソッド(mkdirs)がある。FileUtilsは失敗した場合に例外を出す点がFileクラスと異なる。
            FileUtils.writeStringToFile(fSaveTo, htmlStr);
            System.out.println("Saved to " + fSaveTo.getAbsolutePath());

        }catch(IOException e){
            System.out.println(e);
        }finally{
            //in.close(); ← エラーが出る書き方例。普通にcloseする時は、IOExceptionの処理やらnullチェックやらが必要。
            IOUtils.closeQuietly(reader); // ← 一方 IOUtils なら1行で安全に閉じることができる。
            IOUtils.closeQuietly((InputStream) null);// 閉じるはずのInputStreamがnullでも大丈夫。
        }
    }

    public static void java7PathPractice(){
        // 参照： http://waman.hatenablog.com/entry/20110807/1312677160
        System.out.println("--- Java7 Path Practice ---");
        Path path = Paths.get("./dummyDir1/dummyDir2/../../"); // 実質「./」と同じだが、遠回りルート。
        Path normPath = path.normalize();
        System.out.println("before normalize: " + path.toAbsolutePath()); // 遠回りルートをそのまま出力。
        System.out.println("after  normalize: " + normPath.toAbsolutePath()); // 最短ルートになる。
        System.out.println("");
    }
}
